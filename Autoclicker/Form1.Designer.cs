﻿namespace Autoclicker
{
    partial class Autoclicker
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Autoclicker));
            this.trackbarInterval = new System.Windows.Forms.TrackBar();
            this.timerClick = new System.Windows.Forms.Timer(this.components);
            this.lblMs = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPos = new System.Windows.Forms.Label();
            this.btnSetFKey = new System.Windows.Forms.Button();
            this.btnSetup = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.comboboxAction = new C_Combobox();
            this.ucOptions = new Options();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // trackbarInterval
            // 
            this.trackbarInterval.LargeChange = 50;
            this.trackbarInterval.Location = new System.Drawing.Point(158, 52);
            this.trackbarInterval.Maximum = 1000;
            this.trackbarInterval.Minimum = 20;
            this.trackbarInterval.Name = "trackbarInterval";
            this.trackbarInterval.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackbarInterval.Size = new System.Drawing.Size(45, 252);
            this.trackbarInterval.TabIndex = 1;
            this.trackbarInterval.TickFrequency = 100;
            this.trackbarInterval.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackbarInterval.Value = 20;
            this.trackbarInterval.ValueChanged += new System.EventHandler(this.trackbarInterval_ValueChanged);
            // 
            // timerClick
            // 
            this.timerClick.Interval = 20;
            this.timerClick.Tick += new System.EventHandler(this.timerClick_Tick);
            // 
            // lblMs
            // 
            this.lblMs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.lblMs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMs.ForeColor = System.Drawing.Color.White;
            this.lblMs.Location = new System.Drawing.Point(12, 65);
            this.lblMs.Name = "lblMs";
            this.lblMs.Size = new System.Drawing.Size(133, 25);
            this.lblMs.TabIndex = 6;
            this.lblMs.Text = "NULL";
            this.lblMs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblState
            // 
            this.lblState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblState.ForeColor = System.Drawing.Color.White;
            this.lblState.Location = new System.Drawing.Point(12, 102);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(133, 25);
            this.lblState.TabIndex = 8;
            this.lblState.Text = "Inactive";
            this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPos
            // 
            this.lblPos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.lblPos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPos.ForeColor = System.Drawing.Color.White;
            this.lblPos.Location = new System.Drawing.Point(12, 141);
            this.lblPos.Name = "lblPos";
            this.lblPos.Size = new System.Drawing.Size(133, 25);
            this.lblPos.TabIndex = 12;
            this.lblPos.Text = "X0, Y0";
            this.lblPos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSetFKey
            // 
            this.btnSetFKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSetFKey.Location = new System.Drawing.Point(12, 227);
            this.btnSetFKey.Name = "btnSetFKey";
            this.btnSetFKey.Size = new System.Drawing.Size(133, 30);
            this.btnSetFKey.TabIndex = 13;
            this.btnSetFKey.Text = "Change Key";
            this.btnSetFKey.UseVisualStyleBackColor = true;
            this.btnSetFKey.Click += new System.EventHandler(this.btnSetFKey_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSetup.Image = global::Autoclicker.Properties.Resources.Setup;
            this.btnSetup.Location = new System.Drawing.Point(12, 180);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(133, 32);
            this.btnSetup.TabIndex = 17;
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnLoad.Location = new System.Drawing.Point(12, 270);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(63, 30);
            this.btnLoad.TabIndex = 18;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSave.Location = new System.Drawing.Point(83, 270);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(62, 30);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // comboboxAction
            // 
            this.comboboxAction.BackColor = System.Drawing.Color.DimGray;
            this.comboboxAction.BorderColor = System.Drawing.Color.White;
            this.comboboxAction.BorderSize = 0;
            this.comboboxAction.Caption = "";
            this.comboboxAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.comboboxAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboboxAction.ForeColor = System.Drawing.Color.White;
            this.comboboxAction.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.comboboxAction.Items.AddRange(new object[] {
            "Left-Click",
            "Right-Click",
            "Left-Hold",
            "Right-Hold"});
            this.comboboxAction.ListBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(228)))), ((int)(((byte)(245)))));
            this.comboboxAction.ListTextColor = System.Drawing.Color.Black;
            this.comboboxAction.Location = new System.Drawing.Point(10, 12);
            this.comboboxAction.MinimumSize = new System.Drawing.Size(100, 30);
            this.comboboxAction.Name = "comboboxAction";
            this.comboboxAction.Size = new System.Drawing.Size(184, 30);
            this.comboboxAction.TabIndex = 22;
            // 
            // ucOptions
            // 
            this.ucOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.ucOptions.Location = new System.Drawing.Point(-4, -3);
            this.ucOptions.Margin = new System.Windows.Forms.Padding(0);
            this.ucOptions.Name = "ucOptions";
            this.ucOptions.Size = new System.Drawing.Size(214, 366);
            this.ucOptions.TabIndex = 23;
            this.ucOptions.Visible = false;
            // 
            // Autoclicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.BackgroundImage = global::Autoclicker.Properties.Resources.ScoproV3;
            this.ClientSize = new System.Drawing.Size(204, 358);
            this.Controls.Add(this.comboboxAction);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.btnSetFKey);
            this.Controls.Add(this.lblPos);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblMs);
            this.Controls.Add(this.trackbarInterval);
            this.Controls.Add(this.ucOptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Autoclicker";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "The Autoclicker";
            ((System.ComponentModel.ISupportInitialize)(this.trackbarInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TrackBar trackbarInterval;
        private System.Windows.Forms.Timer timerClick;
        private Label lblMs;
        private Label lblState;
        private Label lblPos;
        private Button btnSetFKey;
        private Button btnSetup;
        private Button btnLoad;
        private Button btnSave;
        private C_Combobox comboboxAction;
        private Options ucOptions;
    }
}