﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.Design.AxImporter;

namespace Autoclicker
{
    public partial class Options : UserControl
    {
        public Options()
        {
            InitializeComponent();
            PrefHelper.chckboxHold = chckboxHold;
            PrefHelper.chckboxRmode = chckboxRandom;
            PrefHelper.radXmode = radExtreme;
            PrefHelper.radSmode= radSlow;
            PrefHelper.trackbarRStrength = trackbarStrength;
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            Visible = false;
            Enabled = false;
            SendToBack();
        }

        private void chckboxExtreme_CheckedChanged(object sender, EventArgs e)
        {
            PrefHelper.Xmode_Changed(radExtreme.Checked);
        }

        private void chckboxSlowmode_CheckedChanged(object sender, EventArgs e)
        {
            PrefHelper.Smode_Changed(radSlow.Checked);
        }

        private void chckboxRandom_CheckedChanged(object sender, EventArgs e)
        {
            PrefHelper.Rmode_Changed(chckboxRandom.Checked);
            if(chckboxRandom.Checked) 
            { 
                trackbarStrength.Enabled= true;
            }
            else 
            {
                trackbarStrength.Enabled = false;
            }
        }

        /// <summary>
        /// Set the strength of the randomiser.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trackbarStrength_Scroll(object sender, EventArgs e)
        {
            int value = trackbarStrength.Value;
            switch(value)
            {
                case 0:
                    lblRstrength.Text = "Small inperfections";
                    PrefHelper.rStrength = value;
                    break;
                case 1:
                    lblRstrength.Text = "Drunk behavior";
                    PrefHelper.rStrength = value;
                    break;
                case 2:
                    lblRstrength.Text = "Rage kid"; 
                    PrefHelper.rStrength = value;
                    break;
                default:
                    trackbarStrength.Value = 0;
                    lblRstrength.Text = "Small inperfections";
                    PrefHelper.rStrength = 0;
                    break;
            }
        }

        /// <summary>
        /// Enable or disable the mouse-hold-with-interval mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chckboxHold_CheckedChanged(object sender, EventArgs e)
        {
            PrefHelper.enable_hold_time = chckboxHold.Checked;
            if(PrefHelper.trackbarInterval == null) { return; }
            if(!chckboxHold.Checked && PrefHelper.comboboxAction_selectedIndex >=2 )
            {
                PrefHelper.trackbarInterval.Enabled = false;
            }
            else
            { 
                PrefHelper.trackbarInterval.Enabled = true; 
            }
        }
    }
}
