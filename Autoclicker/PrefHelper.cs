﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoclicker
{
    /// <summary>
    /// Used for changing properties of controls and storing the setup values as well as saving or loading them.
    /// </summary>
    static class PrefHelper
    {
        public static bool xmode = false;
        public static bool smode = false;
        public static bool rmode = false;

        public static TrackBar? trackbarInterval;
        public static Label? lblMs;
        public static CheckBox? chckboxHold;
        public static CheckBox? chckboxRmode;
        public static RadioButton? radSmode;
        public static RadioButton? radXmode;
        public static TrackBar? trackbarRStrength;

        public static int rStrength = 0;

        public static bool enable_hold_time = false;
        public static int comboboxAction_selectedIndex;

        public static void Xmode_Changed(bool value)
        {
            if (trackbarInterval == null || lblMs == null) { return; }

            xmode = value;
            if (xmode)
            {
                trackbarInterval.LargeChange = 5;
                //trackbarInterval.TickFrequency = 10;
                trackbarInterval.Minimum = 1;
                trackbarInterval.Maximum = 100;
                trackbarInterval.Value = 1;
                lblMs.Text = "1";
            }
            else
            {
                trackbarInterval.LargeChange = 50;
                //trackbarInterval.TickFrequency = 100;
                trackbarInterval.Minimum = 20;
                trackbarInterval.Maximum = 1000;
                trackbarInterval.Value = 20;
                lblMs.Text = "20";
            }
        }
        public static void Smode_Changed(bool value, int interval = 1000) 
        {
            if (trackbarInterval == null || lblMs == null) { return; }

            smode = value;
            if (smode)
            {
                trackbarInterval.LargeChange = 500;
                //trackbarInterval.TickFrequency = 250;
                trackbarInterval.Minimum = 1000;
                trackbarInterval.Maximum = 5000;
                trackbarInterval.Value = interval;
                lblMs.Text = interval.ToString();
            }
            else
            {
                trackbarInterval.LargeChange = 50;
                //trackbarInterval.TickFrequency = 100;
                trackbarInterval.Minimum = 20;
                trackbarInterval.Maximum = 1000;
                trackbarInterval.Value = 20;
                lblMs.Text = "20";
            }
        }
        public static void Rmode_Changed(bool value)
        {
            rmode= value;
        }

        public static bool setControlValues(Preferences p)
        {
            try
            {
                if (chckboxHold != null && radSmode != null && chckboxRmode != null && radSmode != null && trackbarRStrength != null && radXmode != null) 
                {
                    chckboxHold.Checked = p.Hold_duration_mode;
                    chckboxRmode.Checked = p.Rmode;
                    radSmode.Checked = p.Smode;
                    radXmode.Checked = p.Xmode;
                    trackbarRStrength.Value = p.Rstrength;
                }              
            } 
            catch { return false; }
            return true;
        }

        public static void RestoreDefaultValues()
        {
            if (chckboxHold != null && radSmode != null && chckboxRmode != null && radSmode != null && trackbarRStrength != null && radXmode != null)
            {
                chckboxHold.Checked = false;
                chckboxRmode.Checked = false;
                radSmode.Checked = false;
                radXmode.Checked = false;
                trackbarRStrength.Value = 0;
            }
        }
    }

}
