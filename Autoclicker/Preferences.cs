﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoclicker
{
    /// <summary>
    /// Actual preferences object to serialize or deserialize.
    /// </summary>
    internal class Preferences
    {

        public bool Rmode { get; set; }
        public bool Xmode { get; set; }
        public bool Smode { get; set; }
        public int Rstrength { get; set; }
        public int Action_index { get; set; }
        public int Interval { get; set; }
        public bool Hold_duration_mode { get; set; }

        public Preferences(bool rmode, int rstrength, bool xmode, bool smode, int interval, int action_index, bool hold_duration_mode) 
        {
            this.Rmode = rmode;
            this.Xmode = xmode;
            this.Smode = smode;
            this.Interval = interval;
            this.Action_index = action_index;
            this.Rstrength= rstrength;
            this.Hold_duration_mode= hold_duration_mode;
        }
    }
}
