﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Autoclicker
{
    /// <summary>
    /// Serialize and or deserialize json.
    /// </summary>
    static class JsonFileUtils
    {
        private static readonly JsonSerializerOptions _options = new() { DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull };
        public static void StreamWrite(object obj, string file_name)
        {
            using var fileStream = File.Create(file_name);
            using var utf8JsonWriter = new Utf8JsonWriter(fileStream);
            {
                JsonSerializer.Serialize(utf8JsonWriter, obj);
            }
        }
        public static T? Read<T>(string file_path)
        {
            string text = File.ReadAllText(file_path);
            return JsonSerializer.Deserialize<T>(text);
        }
    }
}
