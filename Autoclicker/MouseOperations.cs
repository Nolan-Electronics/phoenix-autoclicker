﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Autoclicker
{
    public class MouseOperations
    {
        [Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public static void SetCursorPosition(int x, int y)
        {
            SetCursorPos(x, y);
        }
        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition(bool random)
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if(!gotPoint) { currentMousePoint = new MousePoint(0, 0); }

            if(random)
            {
                Random r = new Random();

                currentMousePoint.X += r.Next(10 + (12 * PrefHelper.rStrength));
                currentMousePoint.Y += r.Next(10 + (12 * PrefHelper.rStrength));
            }

            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value, bool random, Label? posLabel)
        {
            MousePoint position = GetCursorPosition(random);
            if(posLabel != null) { posLabel.Text= $"X{position.X}, Y{position.Y}"; }
            mouse_event(
                (int)value,
                position.X,
                position.Y,
                0,
                0
            );
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePoint
        {
            public int X;
            public int Y;

            public MousePoint(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

    }
}
