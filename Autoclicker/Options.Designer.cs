﻿namespace Autoclicker
{
    partial class Options
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chckboxRandom = new System.Windows.Forms.CheckBox();
            this.btnSetup = new System.Windows.Forms.Button();
            this.radExtreme = new System.Windows.Forms.RadioButton();
            this.radSlow = new System.Windows.Forms.RadioButton();
            this.lblRstrength = new System.Windows.Forms.Label();
            this.trackbarStrength = new System.Windows.Forms.TrackBar();
            this.chckboxHold = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarStrength)).BeginInit();
            this.SuspendLayout();
            // 
            // chckboxRandom
            // 
            this.chckboxRandom.AutoSize = true;
            this.chckboxRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.chckboxRandom.ForeColor = System.Drawing.Color.White;
            this.chckboxRandom.Location = new System.Drawing.Point(68, 150);
            this.chckboxRandom.Name = "chckboxRandom";
            this.chckboxRandom.Size = new System.Drawing.Size(85, 24);
            this.chckboxRandom.TabIndex = 16;
            this.chckboxRandom.Text = "R-Mode";
            this.chckboxRandom.UseVisualStyleBackColor = true;
            this.chckboxRandom.CheckedChanged += new System.EventHandler(this.chckboxRandom_CheckedChanged);
            // 
            // btnSetup
            // 
            this.btnSetup.BackColor = System.Drawing.Color.White;
            this.btnSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSetup.Location = new System.Drawing.Point(41, 257);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(133, 32);
            this.btnSetup.TabIndex = 18;
            this.btnSetup.Text = "Close";
            this.btnSetup.UseVisualStyleBackColor = false;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // radExtreme
            // 
            this.radExtreme.AutoSize = true;
            this.radExtreme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radExtreme.ForeColor = System.Drawing.Color.White;
            this.radExtreme.Location = new System.Drawing.Point(68, 20);
            this.radExtreme.Name = "radExtreme";
            this.radExtreme.Size = new System.Drawing.Size(83, 24);
            this.radExtreme.TabIndex = 19;
            this.radExtreme.TabStop = true;
            this.radExtreme.Text = "X-Mode";
            this.radExtreme.UseVisualStyleBackColor = true;
            this.radExtreme.CheckedChanged += new System.EventHandler(this.chckboxExtreme_CheckedChanged);
            // 
            // radSlow
            // 
            this.radSlow.AutoSize = true;
            this.radSlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radSlow.ForeColor = System.Drawing.Color.White;
            this.radSlow.Location = new System.Drawing.Point(68, 48);
            this.radSlow.Name = "radSlow";
            this.radSlow.Size = new System.Drawing.Size(83, 24);
            this.radSlow.TabIndex = 20;
            this.radSlow.TabStop = true;
            this.radSlow.Text = "S-Mode";
            this.radSlow.UseVisualStyleBackColor = true;
            this.radSlow.CheckedChanged += new System.EventHandler(this.chckboxSlowmode_CheckedChanged);
            // 
            // lblRstrength
            // 
            this.lblRstrength.AutoSize = true;
            this.lblRstrength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRstrength.ForeColor = System.Drawing.Color.White;
            this.lblRstrength.Location = new System.Drawing.Point(46, 181);
            this.lblRstrength.Name = "lblRstrength";
            this.lblRstrength.Size = new System.Drawing.Size(143, 20);
            this.lblRstrength.TabIndex = 22;
            this.lblRstrength.Text = "Small inperfections";
            this.lblRstrength.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackbarStrength
            // 
            this.trackbarStrength.Enabled = false;
            this.trackbarStrength.LargeChange = 1;
            this.trackbarStrength.Location = new System.Drawing.Point(41, 202);
            this.trackbarStrength.Maximum = 2;
            this.trackbarStrength.Name = "trackbarStrength";
            this.trackbarStrength.Size = new System.Drawing.Size(133, 45);
            this.trackbarStrength.TabIndex = 23;
            this.trackbarStrength.Scroll += new System.EventHandler(this.trackbarStrength_Scroll);
            // 
            // chckboxHold
            // 
            this.chckboxHold.AutoSize = true;
            this.chckboxHold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.chckboxHold.ForeColor = System.Drawing.Color.White;
            this.chckboxHold.Location = new System.Drawing.Point(47, 98);
            this.chckboxHold.Name = "chckboxHold";
            this.chckboxHold.Size = new System.Drawing.Size(127, 24);
            this.chckboxHold.TabIndex = 24;
            this.chckboxHold.Text = "Hold-Duration";
            this.chckboxHold.UseVisualStyleBackColor = true;
            this.chckboxHold.CheckedChanged += new System.EventHandler(this.chckboxHold_CheckedChanged);
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.Controls.Add(this.chckboxHold);
            this.Controls.Add(this.trackbarStrength);
            this.Controls.Add(this.lblRstrength);
            this.Controls.Add(this.radSlow);
            this.Controls.Add(this.radExtreme);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.chckboxRandom);
            this.Name = "Options";
            this.Size = new System.Drawing.Size(220, 397);
            ((System.ComponentModel.ISupportInitialize)(this.trackbarStrength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CheckBox chckboxRandom;
        private Button btnSetup;
        private RadioButton radExtreme;
        private RadioButton radSlow;
        private Label lblRstrength;
        private TrackBar trackbarStrength;
        private CheckBox chckboxHold;
    }
}
