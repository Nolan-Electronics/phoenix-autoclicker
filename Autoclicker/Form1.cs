using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using static Autoclicker.MouseOperations;

namespace Autoclicker
{
    public partial class Autoclicker : Form
    {
        Point targetpoint = new Point(0,0);
        const int MOUSEEVENTF_LEFTDOWN = 2;
        const int MOUSEEVENTF_LEFTUP = 4;
        const int INPUT_MOUSE = 0;

        private KeyHandler ghk;
        Keys keyPressed;

        private int initial_tick_freq;
        private int timer_tick_index = 0;
        public bool isActive = false;

        int[] flags = new int[] { (int) MouseOperations.MouseEventFlags.LeftDown, (int) MouseOperations.MouseEventFlags.LeftUp };

        string file_name = "autoclicker_preferences.json";

        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int dwFlags;
            public int time;
            public IntPtr dwExtraInfo;
        }

        public struct INPUT
        {
            public uint type;
            public MOUSEINPUT mi;
        }

        public Autoclicker()
        {
            InitializeComponent();
            comboboxAction.OnSelectedIndexChanged += new System.EventHandler(ComboboxAction_SelectedIndexChanged);
            lblMs.Text = trackbarInterval.Value.ToString();
            ghk = new KeyHandler(Keys.F6, this);
            ghk.Register();
            PrefHelper.lblMs = lblMs;
            PrefHelper.trackbarInterval = trackbarInterval;
            comboboxAction.SelectedIndex = 0;
        }

        /// <summary>
        /// This code will be executed if the key, that is registerd as a hotkey, is pressed.
        /// From here depending on the mode/action to perform, either the timer will be started or the mouseevent is directly triggered.
        /// </summary>
        private void HandleHotKey()
        {
            switch(comboboxAction.SelectedIndex)
            {
                case 0:
                    flags = new int[] { (int)MouseOperations.MouseEventFlags.LeftDown, (int)MouseOperations.MouseEventFlags.LeftUp };
                    HandleTimer(isActive);
                    break;
                case 1:
                    flags = new int[] { (int)MouseOperations.MouseEventFlags.RightDown, (int)MouseOperations.MouseEventFlags.RightUp };
                    HandleTimer(isActive);
                    break;
                case 2:
                    flags = new int[] { (int)MouseOperations.MouseEventFlags.LeftDown, (int)MouseOperations.MouseEventFlags.LeftUp };
                    if (!isActive) { MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[0], PrefHelper.rmode, lblPos); }
                    else { MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[1], PrefHelper.rmode, null); }

                    break;
                case 3:
                    flags = new int[] { (int)MouseOperations.MouseEventFlags.RightDown, (int)MouseOperations.MouseEventFlags.RightUp };
                    if (!isActive) { MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[0], PrefHelper.rmode, lblPos); }
                    else { MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[1], PrefHelper.rmode, null); }
                    break;
            }

            isActive = SetLabelState(isActive);

            bool SetLabelState(bool isActive)
            {
                if (isActive)
                {
                    lblState.Text = "Disbaled";
                    lblState.ForeColor = Color.White;
                    return false;
                }
                else
                {
                    lblState.Text = "Active";
                    lblState.ForeColor = Color.FromArgb(255, 128, 0);
                    return true;
                }
            }
            void HandleTimer(bool isActive)
            {
                if (isActive)
                {
                    timerClick.Stop();
                }
                else
                {
                    timerClick.Interval = trackbarInterval.Value;
                    initial_tick_freq = timerClick.Interval;
                    timerClick.Start();
                }
            }

            //Re-Send the start/stop keystroke to the active application because it has been interrupted.
            SendKeys.Send(keyPressed.ToString());
        }

        /// <summary>
        /// Listen for start/stop keystroke
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if(m.Msg == Constants.WM_HOTKEY_MSG_ID) 
            {
                HandleHotKey();
            }
            base.WndProc(ref m);
        }

        private void trackbarInterval_ValueChanged(object sender, EventArgs e)
        {
            lblMs.Text = trackbarInterval.Value.ToString();
        }

        /// <summary>
        /// This timer ticks in the interval wich the click has to be performed.
        /// The actual clicking is triggered here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerClick_Tick(object sender, EventArgs e)
        {
            timer_tick_index++;
            MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[0], PrefHelper.rmode, lblPos);
            MouseOperations.MouseEvent((MouseOperations.MouseEventFlags)flags[0], PrefHelper.rmode, null);

            //Let the timer modify its own tick speed by random...
            if (PrefHelper.rStrength > 0 && timer_tick_index > (1000 - trackbarInterval.Value))
            {
                Random random = new Random();
                int x = (random.Next(5) - random.Next(5)) * PrefHelper.rStrength;
                int new_interval = timerClick.Interval += x;
                if (new_interval > initial_tick_freq + 35 || new_interval < initial_tick_freq - 35)
                {
                    timerClick.Interval = initial_tick_freq;
                }
                else
                {
                    timerClick.Interval += new_interval;
                }
                timer_tick_index = 0;
            }
        }

        /// <summary>
        /// Set the start/stop key to a new value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetFKey_KeyDown(object? sender, KeyEventArgs? e)
        {
            if(e != null) { keyPressed = e.KeyCode; }
            btnSetFKey.Text = "Change Key";
            ghk.Unregister();
            ghk = new KeyHandler(keyPressed, this);
            ghk.Register();
            btnSetFKey.BackColor = SystemColors.Control;
        }

        /// <summary>
        /// Listen for new start/stop key.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetFKey_Click(object sender, EventArgs e)
        {
            btnSetFKey.Text = "Enter Key...";
            btnSetFKey.KeyDown += new KeyEventHandler(btnSetFKey_KeyDown);
            btnSetFKey.BackColor = Color.LightBlue;
         }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            ucOptions.BringToFront();
            ucOptions.Visible = true;
            ucOptions.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Preferences p = new Preferences(PrefHelper.rmode, PrefHelper.rStrength, PrefHelper.xmode, PrefHelper.smode, trackbarInterval.Value, comboboxAction.SelectedIndex, PrefHelper.enable_hold_time);
            string file_name = "autoclicker_preferences.json";
            JsonFileUtils.StreamWrite(p, file_name);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                Preferences? p = JsonFileUtils.Read<Preferences>(file_name);
                if (p == null) { return; }
                comboboxAction.SelectedIndex = p.Action_index;
                PrefHelper.Smode_Changed(true, p.Interval);
                if (!PrefHelper.setControlValues(p))
                {
                    PrefHelper.RestoreDefaultValues();
                }
            }
            catch (Exception)
            {
                comboboxAction.SelectedIndex = 0;
                trackbarInterval.Value = 20;
                return;
            }
            this.Invalidate();
        }

        /// <summary>
        /// Enable or disable controls based on the mode/action to perform.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboboxAction_SelectedIndexChanged(object? sender, EventArgs? e)
        {
            int selected_index = comboboxAction.SelectedIndex;
            PrefHelper.comboboxAction_selectedIndex = selected_index;
            bool isHoldMode = selected_index >= 2;
            if(PrefHelper.chckboxHold != null) { PrefHelper.chckboxHold.Enabled = isHoldMode; }
            if(isHoldMode && !PrefHelper.enable_hold_time)
            {
                trackbarInterval.Enabled = false;
            }
            else
            {
                trackbarInterval.Enabled = true;
            }
        }
    }
}