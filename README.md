- Phoenix Autoclicker

Download ZIP with executable: https://gitlab.com/Nolan-Electronics/phoenix-autoclicker/-/raw/master/Autoclicker_Exe-1.0.zip?inline=false

Speed/Interval:

Set the interval in ms for each click with the trackbar.

If you want faster or slower intervals, check out the S-Mode (Slow) and X-Mode (Extremely fast) options in the settings menu.
But note, that the X-Mode settings probably will not end up be equal to 1ms clicks due to the processing speed and limitations of both the autoclicker itself as well as the software you want to use it in.


R-Mode:

The R-Mode option randomises the exact location (x,y) of the clicks in a range of 10px, 22px and 46px.
The strength of randomisation is depending of the value you set in the Trackbar below the R-Mode option.
"Drunk behavior" and "Rage kid" value will also randomise the exact interval of clicks in a range of -75ms to +75ms.


Please do not use this software in online games which have a no-cheat policy.
